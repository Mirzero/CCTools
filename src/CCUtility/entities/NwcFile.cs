﻿namespace CCUtility.Entities
{
    public class NwcFile
    {
        public CCTocEntry TocEntry { get; internal set; }
        public MetaEntry? MetaData { get; internal set; }
        public MetaFileType FileType { get; internal set; }
        public byte[] FileBytes { get; internal set; }
    }
}