﻿// see https://xeen.fandom.com/wiki/CC_File_Format
// Mostly we're doing this dirty, because all we want to do is yoink all the original assets out of the CC file
// for our own usage.
// The CC files are never going to change, given it's been 30 years.

namespace CCUtility.Entities
{
    public class CCTocEntry
    {
        public int Hash { get; set; }
        public int Offset { get; set; }
        public int FileLength { get; set; }
        public int Padding { get; set; }
    }
}