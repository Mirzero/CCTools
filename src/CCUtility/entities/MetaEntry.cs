﻿using System.Text.Json.Serialization;
using CCUtility.JsonConverters;

namespace CCUtility.Entities
{
    public class MetaEntry
    {
        public string Name { get; set; }
        public string Type { get; set; }
        [JsonConverter(typeof(HexConverter))]
        public int Hash { get; set; }
        [JsonConverter(typeof(DecimalConverter))]
        public int Size { get; set; }
        public string Description { get; set; }
    }
}
