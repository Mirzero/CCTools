﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCUtility.Entities
{
    public enum MetaFileType
    {
        Unknown = 1,
        Sprite_Object = 2,
        Sprite_MonsterIdle = 3,
        Sprite_MonsterAttack = 4,
        Sprite_Picture = 5,
        SaveGameData = 6,
        Environment_Sky = 7,
        Environment_MapTiles = 8,
        Environment_WallFront = 9,
        Environment_Ground = 10,
        Environment_WallSide = 11,
    }
}
