﻿using System.Text.Json.Serialization;
using System.Text.Json;

namespace CCUtility.JsonConverters
{
    public class DecimalConverter : JsonConverter<int>
    {
        public override int Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            var value = reader.GetString();
            return Convert.ToInt32(value, 10);
        }

        public override void Write(Utf8JsonWriter writer, int value, JsonSerializerOptions options)
        {
            throw new NotImplementedException();
        }
    }
}
