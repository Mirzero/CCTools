﻿using System.Text.Json.Serialization;
using System.Text.Json;

namespace CCUtility.JsonConverters
{
    public class HexConverter : JsonConverter<int>
    {
        // see https://stackoverflow.com/a/70171728/495000

        public override int Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            var value = reader.GetString();
            return Convert.ToInt32(value, 16);
        }

        public override void Write(Utf8JsonWriter writer, int value, JsonSerializerOptions options)
        {
            throw new NotImplementedException();
        }
    }
}
