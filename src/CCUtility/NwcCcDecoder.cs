﻿using System.Text.Json;
using CCUtility.Entities;

// see https://xeen.fandom.com/wiki/CC_File_Format
// Mostly we're doing this dirty, because all we want to do is yoink all the original assets out of the CC file
// for our own usage.
// The CC files are never going to change, given it's been 30 years.

namespace CCUtility
{
    public class NwcCcDecoder
    {
        public static readonly int TocEntryBytes = 8;
        public static readonly byte EncryptionMask = 0x35;

        public List<NwcFile> ExtractDecodedFiles(string ccFileName, string metaFileName)
        {
            var metaList = JsonSerializer.Deserialize<List<MetaEntry>>(File.ReadAllText(metaFileName));

            using (var ccFile = new BinaryReader(File.OpenRead(ccFileName)))
            {
                var fileCount = ccFile.ReadUInt16();
                var tocEntries = GetDecodedTocEntries(ccFile, fileCount);

                var nwcFiles = tocEntries.Select(t => 
                {
                    var metaData = metaList.FirstOrDefault(m => m.Hash == t.Hash);

                    return new NwcFile 
                    {
                        TocEntry = t,
                        MetaData = metaData,
                        FileBytes = GetDecodedFileBytes(ccFile, t.Offset, t.FileLength),
                        FileType = NwcCcDecoderHelpers.GetFileType(metaData.Name),
                    };
                });

                return nwcFiles.ToList();
            }
        }

        private List<CCTocEntry> GetDecodedTocEntries(BinaryReader ccFile, int fileCount)
        {
            var tocRawSize = fileCount * TocEntryBytes;
            var tocRaw = ccFile.ReadBytes(tocRawSize);
            int ah = 0xac;
            for (int i = 0; i < tocRaw.Length; i++)
            {
                tocRaw[i] = (byte)(((tocRaw[i] << 2 | tocRaw[i] >> 6) + ah) & 0xff);
                ah += 0x67;
            }

            var tocEntries = new List<CCTocEntry>();
            for (int i = 0; i < fileCount; i++)
            {
                var index = i * TocEntryBytes;

                tocEntries.Add(new CCTocEntry
                {
                    Hash =       BitConverter.ToInt32([tocRaw[index  ], tocRaw[index+1],               0,           0]),
                    Offset =     BitConverter.ToInt32([tocRaw[index+2], tocRaw[index+3], tocRaw[index+4],           0]),
                    FileLength = BitConverter.ToInt32([tocRaw[index+5], tocRaw[index+6],               0,           0]),
                    Padding =    BitConverter.ToInt32([tocRaw[index+7],               0,               0,           0]),
                });
            }

            return tocEntries;
        }

        private byte[] GetDecodedFileBytes(BinaryReader ccFile, int offset, int length)
        {
            ccFile.BaseStream.Seek(offset, SeekOrigin.Begin);
            var fileBytes = ccFile.ReadBytes(length);
            for (int i = 0; i < fileBytes.Length; i++)
            {
                fileBytes[i] ^= EncryptionMask;
            }

            return fileBytes;
        }
    }
}
