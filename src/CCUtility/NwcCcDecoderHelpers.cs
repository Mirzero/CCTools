﻿using CCUtility.Entities;

internal static class NwcCcDecoderHelpers
{

    public static MetaFileType GetFileType(string name)
    {
        var ext = Path.GetExtension(name).ToUpper();
        switch (ext)
        {
            case ".SPL":
                return MetaFileType.SaveGameData;
            case ".ATT":
                return MetaFileType.Sprite_MonsterAttack;
            case ".MON":
                return MetaFileType.Sprite_MonsterIdle;
            case ".OBJ":
            case ".0BJ":
                return MetaFileType.Sprite_Object;
            case ".PIC":
                return MetaFileType.Sprite_Picture;
            case ".BUF":
                return MetaFileType.Unknown;
            case ".HED":
                return MetaFileType.Unknown;
            case ".TXT":
                return MetaFileType.Unknown;
            case ".VOC":
                return MetaFileType.Unknown;
            case ".BIN":
                return MetaFileType.Unknown;
            case ".ICN":
                return MetaFileType.Unknown;
            case ".RAW":
                return MetaFileType.Unknown;
            case ".M":
                return MetaFileType.Unknown;
            case ".END":
                return MetaFileType.Unknown;
            case ".TWN":
                return MetaFileType.Unknown;
            case ".VGA":
                return MetaFileType.Unknown;
            case ".GND":
                return MetaFileType.Environment_Ground;
            case ".SKY":
                return MetaFileType.Environment_Sky;
            case ".TIL":
                return MetaFileType.Environment_MapTiles;
            case ".FAC":
                return MetaFileType.Unknown;
            case ".SRF":
                return MetaFileType.Unknown;
            case ".WAL":
                return MetaFileType.Unknown;
            case ".PAL":
                return MetaFileType.Unknown;
            case ".FWL":
                return MetaFileType.Environment_WallFront;
            case ".BRD":
                return MetaFileType.Unknown;
            case ".FRM":
                return MetaFileType.Unknown;
            case ".SWL":
                return MetaFileType.Environment_WallSide;
            case ".OUT":
                return MetaFileType.Unknown;
            default:
                return MetaFileType.Unknown;
        }
    }
}