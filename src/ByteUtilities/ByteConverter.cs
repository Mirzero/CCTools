﻿namespace ByteUtilities
{
    public static class ByteConverter
    {
        public static int LSBBytesToInt(byte[] source, int offset, int byteCount)
        {
            switch (byteCount)
            {
                case 1:
                    return BitConverter.ToInt32([source[offset], 0, 0, 0]);
                case 2:
                    return BitConverter.ToInt32([source[offset], source[offset+1], 0, 0]);
                case 3:
                    return BitConverter.ToInt32([source[offset], source[offset + 1], source[offset + 1], 0]);
                case 4:
                    return BitConverter.ToInt32([source[offset], source[offset + 1], source[offset + 2], source[offset + 3]]);
                default:
                    throw new InvalidDataException();
            }
        }
    }
}
