﻿public class NwcSpriteFrameHeader
{
    public int FirstCellOffset { get; set; }
    public int SecondCellOffset { get; set; }
}