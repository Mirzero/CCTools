﻿using System.Diagnostics;
using CCUtility.Entities;
using ByteUtilities;
using NwcSpriteConverter;
using System.Reflection.PortableExecutable;

public static class ProgramUtils
{
    public static readonly string FileHeader = $"|{"FileId",10}|{"Offset",10}|{"FileLength",10}|{"Padding",10}|{"Filename",10}|{"FileType",10}|";

    public static string FileString(this NwcFile file)
    {
        return $"|{file.MetaData.Hash.ToString("X4"),10}|{file.TocEntry.Offset,10}|{file.TocEntry.FileLength,10}|{file.TocEntry.Padding,10}|{file.MetaData.Name,10}|{file.FileType,10}|";
    }
}

public class Program
{
    private static void Main(string[] args)
    {
        var ccFile = "E:\\Projects\\Programming\\ArmiesOfAlamar\\XeenExtraction\\XEEN.CC";
        var ccMetaFile = "E:\\Projects\\Programming\\ArmiesOfAlamar\\CCTools\\src\\CCExtractor\\WikiMetadata\\wikiMeta_Xeen_CC.json";
        var outputPath = "E:\\Projects\\Programming\\ArmiesOfAlamar\\CCTools\\extracted";


        var ccu = new CCUtility.NwcCcDecoder();

        Console.Write("Decoding and Extracting CC File... ");
        var decodeTime = Stopwatch.StartNew();
        var files = ccu.ExtractDecodedFiles(ccFile, ccMetaFile);
        decodeTime.Stop();
        Console.WriteLine($" ...extracted {files.Count} files in in {decodeTime.ElapsedMilliseconds / 1000} seconds");

        Console.WriteLine(ProgramUtils.FileHeader);

        var groundFiles = files
            .Where(f => f.FileType == MetaFileType.Environment_Ground)
            .ToList();

        var groundPathFragment = "Sprites\\Ground";
        var groundPathFull = Path.Combine(outputPath, groundPathFragment);

        groundFiles.ForEach(f =>
        {
            Console.WriteLine(f.FileString());

            var frameCount = ByteConverter.LSBBytesToInt(f.FileBytes, 0, SpriteDataByteSizes.FrameCountSize);
            Console.WriteLine($"frameCount: {frameCount}");

            var frameHeaders = new List<NwcSpriteFrameHeader>();
            for (int i = 0; i < frameCount; i++)
            {
                var frameHeaderOffset = SpriteDataByteSizes.FrameCountSize + (i * SpriteDataByteSizes.FrameHeaderSize);

                var header = new NwcSpriteFrameHeader
                {
                    FirstCellOffset = ByteConverter.LSBBytesToInt(f.FileBytes, frameHeaderOffset, SpriteDataByteSizes.FrameHeaderCellOffsetSize),
                    SecondCellOffset = ByteConverter.LSBBytesToInt(f.FileBytes, frameHeaderOffset + 1, SpriteDataByteSizes.FrameHeaderCellOffsetSize)
                };
                frameHeaders.Add(header);

                Console.WriteLine($"frameHeader({i}): firstCellOffset: {header.FirstCellOffset} secondCellOffset: {header.SecondCellOffset}");
            }

            foreach (var (frameHeader, i) in frameHeaders.Select((value, i) => (value, i)))
            {
                var cellHeader1 = new NwcSpriteCellHeader 
                { 
                    XOffset = ByteConverter.LSBBytesToInt(f.FileBytes, frameHeader.FirstCellOffset + SpriteDataByteSizes.CellHeaderXOffset_Offset, SpriteDataByteSizes.CellHeaderXOffset_Size),
                    XLength = ByteConverter.LSBBytesToInt(f.FileBytes, frameHeader.FirstCellOffset + SpriteDataByteSizes.CellHeaderXLength_Offset, SpriteDataByteSizes.CellHeaderXLength_Size),
                    YOffset = ByteConverter.LSBBytesToInt(f.FileBytes, frameHeader.FirstCellOffset + SpriteDataByteSizes.CellHeaderYOffset_Offset, SpriteDataByteSizes.CellHeaderYOffset_Size),
                    YLength = ByteConverter.LSBBytesToInt(f.FileBytes, frameHeader.FirstCellOffset + SpriteDataByteSizes.CellHeaderYLength_Offset, SpriteDataByteSizes.CellHeaderYLength_Size),
                };
                Console.WriteLine($"frameHeader({i}): cellHeader1: xOffset: {cellHeader1.XOffset} xLength: {cellHeader1.XLength} yOffset: {cellHeader1.YOffset} yLength: {cellHeader1.YLength}");

                var cellHeader2 = new NwcSpriteCellHeader
                {
                    XOffset = ByteConverter.LSBBytesToInt(f.FileBytes, frameHeader.SecondCellOffset + SpriteDataByteSizes.CellHeaderXOffset_Offset, SpriteDataByteSizes.CellHeaderXOffset_Size),
                    XLength = ByteConverter.LSBBytesToInt(f.FileBytes, frameHeader.SecondCellOffset + SpriteDataByteSizes.CellHeaderXLength_Offset, SpriteDataByteSizes.CellHeaderXLength_Size),
                    YOffset = ByteConverter.LSBBytesToInt(f.FileBytes, frameHeader.SecondCellOffset + SpriteDataByteSizes.CellHeaderYOffset_Offset, SpriteDataByteSizes.CellHeaderYOffset_Size),
                    YLength = ByteConverter.LSBBytesToInt(f.FileBytes, frameHeader.SecondCellOffset + SpriteDataByteSizes.CellHeaderYLength_Offset, SpriteDataByteSizes.CellHeaderYLength_Size),
                };
                Console.WriteLine($"frameHeader({i}): cellHeader2: xOffset: {cellHeader2.XOffset} xLength: {cellHeader2.XLength} yOffset: {cellHeader2.YOffset} yLength: {cellHeader2.YLength}");
            }

            //Directory.CreateDirectory(outFolderPath);
            //var outFilePath = Path.Combine(outFolderPath, f.MetaData.Name);
            //var outFileStream = File.Open(outFilePath,  FileMode.Create);
            //outFileStream.Write(f.FileBytes);
        });
    }
}


