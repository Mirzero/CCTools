﻿public class NwcSpriteCellHeader
{
    public int XOffset { get; set; }
    public int XLength { get; set; }
    public int YOffset { get; set; }
    public int YLength { get; set; }
}