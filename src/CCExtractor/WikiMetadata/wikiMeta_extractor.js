// This JS script can be used to extract CC file meta data from the Xeen Fandom Wiki as JSON
// go to https://xeen.fandom.com/wiki/Filenames and paste it into the console and run it
// table converter function from here: https://gist.github.com/johannesjo/6b11ef072a0cb467cc93a885b5a1c19f

function parseHTMLTableElem(tableEl) {
    const columns = Array.from(tableEl.querySelectorAll('th')).map(it => it.textContent)
    const rows = tableEl.querySelectorAll('tbody > tr')
    return Array.from(rows).map(row => {
        const cells = Array.from(row.querySelectorAll('td'))
        return columns.reduce((obj, col, idx) => {
            obj[col] = cells[idx].textContent
            return obj
        }, {})
    })
}

JSON.stringify(parseHTMLTableElem(document.querySelectorAll('table[data-index-number="1"]')[0]))