﻿namespace NwcSpriteConverter
{

    // FrameCount (2) = N
    //   Frame1Header (4)
    //     Frame1Cell1Offset (2)
    //     Frame1Cell2Offset (2)
    //   FrameHeader2 (4)
    //   ...
    //   FrameHeaderN (4)
    //   Frame1Cell1
    //     Frame1Cell1Header (8)
    //       Frame1Cell1HeaderXOffset (2)
    //       Frame1Cell1HeaderXLength (2)
    //       Frame1Cell1HeaderYOffset (2)
    //       Frame1Cell1HeaderYLength (2)
    //     Frame1Cell1Data (var)
    //   Frame1Cell2
    //   ...
    //   FrameNCell1
    //   FrameNCell2

    public static class SpriteDataByteSizes
    {
        public static readonly int FrameCountSize = 2;
        public static readonly int FrameHeaderSize = 4;
        public static readonly int FrameHeaderCellOffsetSize = 2;

        public static readonly int CellHeaderSize = 8;
        public static readonly int CellHeaderXOffset_Size = 2;
        public static readonly int CellHeaderXLength_Size = 2;
        public static readonly int CellHeaderYOffset_Size = 2;
        public static readonly int CellHeaderYLength_Size = 2;
        public static readonly int CellHeaderXOffset_Offset = 0;
        public static readonly int CellHeaderXLength_Offset = 2;
        public static readonly int CellHeaderYOffset_Offset = 4;
        public static readonly int CellHeaderYLength_Offset = 6;
    }
}
